
package com.nabenik;


import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.eclipse.microprofile.config.inject.ConfigProperty;

@Path("/hello")
public class GreetResource {
   
    @Inject
    @ConfigProperty(defaultValue = "A Service", name = "extmessage")
    String message;
    
    @GET
    public String getDefaultMessage() {
        return "Hello from 0 service " + message;
    }
}
